package dk.mehmedbasic.originalxboxtools.eeprom

import dk.mehmedbasic.originalxboxtools.cli.HelpFormatter
import dk.mehmedbasic.originalxboxtools.util.HexFormat
import groovy.transform.TypeChecked
import joptsimple.OptionException
import joptsimple.OptionParser

@TypeChecked
class EepromTool {
    private static OptionParser createParser() {
        def parser = new OptionParser()

        def stdin = parser.accepts("stdin", "Read the EEPROM from standard input")
        parser.accepts("i", "Path to EEPROM file").requiredUnless("stdin").withRequiredArg().ofType(File)
        stdin.requiredUnless("i")

        parser.accepts("light", "Don't print version and copyright info (useful for scripts).")
        parser.accepts("help", "Show this help screen.")

        def all = parser.accepts("all", "Prints all known information")
        parser.mutuallyExclusive(all, parser.accepts("hdd", "Prints the HDD key"))
        parser.mutuallyExclusive(all, parser.accepts("xbe", "Prints the XBE region"))
        parser.mutuallyExclusive(all, parser.accepts("dvd", "Prints the DVD zone"))
        parser.mutuallyExclusive(all, parser.accepts("online", "Prints the online key"))
        parser.mutuallyExclusive(all, parser.accepts("video", "Prints the video standard"))
        parser.mutuallyExclusive(all, parser.accepts("serial", "Prints the Xbox serial number"))
        parser.mutuallyExclusive(all, parser.accepts("mac", "Prints the MAC address"))
        parser.mutuallyExclusive(all, parser.accepts("version", "Prints the Xbox version"))

        def formatter = new HelpFormatter("GroovyXbox EEPROM tool")
        formatter.sortOrder += ["i", "stdin", "help", "light", "all", "hdd", "serial", "mac", "version", "online", "video", "dvd", "xbe"]
        formatter.spacesBefore << "help"
        formatter.spacesBefore << "light"

        parser.formatHelpWith(formatter)
        parser
    }

    static void main(String[] args) {
        def string = "c:/src/eeprom.bin"
        println("File: $string")

        def eeprom = new Eeprom(new File(string))
        def result = eeprom.calculateHardDriveKey("VMware Virtual IDE Hard Drive", "00000000000000000001")
        println("Serial number: ${eeprom.serialNumber}")
        println("Password: ${HexFormat.bytesLowerCase(result)}")
        println("Password: ${HexFormat.bytesLowerCase(result).substring(0, 32)}")
        println("Password: ${HexFormat.bytes(result)}")
        println("Password: ${HexFormat.bytes(result).substring(0, 32)}")

        OptionParser parser = createParser()

        if (args.length == 0) {
            //    parser.printHelpOn(System.out)
            return
        }

        try {
            parser.parse(args)
        } catch (OptionException e) {
            println "Wrong arguments: ${e.message}\n"
            //parser.printHelpOn(System.out)
        }

    }


}
