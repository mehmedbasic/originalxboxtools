package dk.mehmedbasic.originalxboxtools.eeprom

import dk.mehmedbasic.originalxboxtools.crypto.Crypto
import dk.mehmedbasic.originalxboxtools.crypto.HmacReset
import dk.mehmedbasic.originalxboxtools.crypto.Rc4
import dk.mehmedbasic.originalxboxtools.util.ArrayConvert

import java.nio.ByteBuffer

/**
 * A Groovy representation of the Xbox EEPROM
 */
class Eeprom {
    private final byte[] raw
    final int version

    private Eeprom(Eeprom eeprom) {
        this.raw = Arrays.copyOf(eeprom.raw, eeprom.size)
        this.version = eeprom.version
    }

    Eeprom(File file) {
        this.raw = file.readBytes()
        this.version = decrypt()
    }

    int getSize() {
        raw.length
    }

    String getSerialNumber() {
        new String(getRawData(Structure.SerialNumber))
    }

    byte[] getMacAddress() {
        getRawData(Structure.MACAddress)
    }

    DvdZone getDvdZone() {
        Structure.DVDPlaybackKitZone.convertTo(DvdZone, this)
    }

    VideoStandard getVideoStandard() {
        Structure.VideoStandard.convertTo(VideoStandard, this)
    }

    XbeRegion getXbeRegion() {
        Structure.XBERegion.convertTo(XbeRegion, this)
    }

    byte[] getConfounder() {
        getRawData(Structure.Confounder)
    }

    byte[] getOnlineKey() {
        getRawData(Structure.OnlineKey)
    }

    byte[] getHddKey() {
        getRawData(Structure.HDDKkey)
    }

    private byte[] getHddKeyForSha1() {
        def buffer = ByteBuffer.wrap(getRawData(Structure.HDDKkey))
        buffer.limit(16)
        buffer.array()
    }

    /**
     * Calculates the hard drive key.
     *
     * @param model the model as returned by the IDE interface, max 32 bytes.
     * @param serialNumber the serial number as returned by the IDE interface, max 20 bytes.
     *
     * @return the calculated key.
     */
    byte[] calculateHardDriveKey(String model, String serialNumber) {
        int[] calculation = Crypto.sha1(hddKeyForSha1, model.trim().bytes, serialNumber.trim().bytes)
        return ArrayConvert.intToByte(calculation)
    }

    byte[] getRawData(Structure data) {
        return Arrays.copyOfRange(raw, data.address, data.address + data.readSize)
    }


    private int decrypt() {
        for (int version : HmacReset.versions()) {
            Eeprom copy = new Eeprom(this)

            int[] baKeyHash = Crypto.hmac(version, [copy.bytes(0, 20)])
            Rc4 rc4 = new Rc4(baKeyHash)

            copy.overwrite(rc4.crypt(copy.bytes(20, 8)), 20, 8)
            copy.overwrite(rc4.crypt(copy.bytes(28, 20)), 28, 20)

            // Calculate the Confirm-Hash
            int[] confirmHash = Crypto.hmac(version, [copy.bytes(20, 8), copy.bytes(28, 20)])

            if (Arrays.equals(ArrayConvert.intToByte(confirmHash), bytes(0, 0x14))) {
                // We found the correct version, overwrite the first 48 bytes of the eeprom with the decrypted values.
                def decrypted = copy.bytes(0, 48)
                this.overwrite(decrypted, 0, 48)
                return version
            }
        }

        throw new IllegalStateException("Could not decrypt the eeprom.")
    }

    private byte[] bytes(int offset, int length) {
        return Arrays.copyOfRange(raw, offset, offset + length)
    }

    private void overwrite(byte[] bytes, int offset, int length) {
        for (int i = 0; i < length; i++) {
            raw[offset + i] = bytes[i]
        }
    }
}
