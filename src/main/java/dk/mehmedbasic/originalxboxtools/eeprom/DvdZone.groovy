package dk.mehmedbasic.originalxboxtools.eeprom

import groovy.transform.CompileStatic

@CompileStatic
enum DvdZone implements MatchesEnum {
    ZONE_NONE(0x00), ZONE1(0x01), ZONE2(0x02), ZONE3(0x03), ZONE4(0x04), ZONE5(0x05), ZONE6(0x06)

    final byte[] bytes = new byte[1]

    DvdZone(int code) {
        bytes[0] = code.byteValue()
    }

    @Override
    boolean matches(byte[] code) {
        return code[0] == bytes[0]
    }
}
