package dk.mehmedbasic.originalxboxtools.eeprom

import groovy.transform.CompileStatic

@CompileStatic
enum XbeRegion implements MatchesEnum {
    XBE_INVALID(0x00), NORTH_AMERICA(0x01), JAPAN(0x02), EURO_AUSTRALIA(0x04)

    final byte[] bytes = new byte[1]

    XbeRegion(int code) {
        bytes[0] = code.byteValue()
    }

    @Override
    boolean matches(byte[] code) {
        return code[0] == bytes[0]
    }
}
