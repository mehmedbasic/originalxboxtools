package dk.mehmedbasic.originalxboxtools.eeprom

import groovy.transform.PackageScope

@PackageScope
trait MatchesEnum {
    abstract byte[] getBytes()

    boolean matches(byte[] code) {
        return Arrays.equals(code, bytes)
    }
}