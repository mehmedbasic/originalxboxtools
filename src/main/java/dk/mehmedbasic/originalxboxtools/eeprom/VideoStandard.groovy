package dk.mehmedbasic.originalxboxtools.eeprom

enum VideoStandard implements MatchesEnum {
    VID_INVALID([0, 0, 0, 0]),
    NTSC_M([0, 1, 64, 0]),
    PAL_I([0, 128, 48, 0])

    final byte[] bytes

    VideoStandard(List<Integer> list) {
        bytes = new byte[list.size()]
        for (int i = 0; i < list.size(); i++) {
            Integer integer = list.get(i);
            bytes[i] = integer.byteValue()
        }
    }
}