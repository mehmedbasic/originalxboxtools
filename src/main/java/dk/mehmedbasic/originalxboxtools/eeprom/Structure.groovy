package dk.mehmedbasic.originalxboxtools.eeprom

import groovy.transform.TypeChecked

/**
 * The data structure of the Xbox EEPROM.
 */
@TypeChecked
enum Structure {
    HMAC_SHA1_Hash(0x0, 20),
    Confounder(0x14, 8),
    HDDKkey(0x1c, 16),
    XBERegion(0x2c, 4),
    Checksum2(0x30, 4),
    SerialNumber(0x34, 12),
    MACAddress(0x40, 6),
    UNKNOWN2(0x46, 2),
    OnlineKey(0x48, 16),
    VideoStandard(0x58, 4),
    UNKNOWN3(0x5C, 4),
    Checksum3(0x60, 4),
    TimeZoneBias(0x64, 4),
    TimeZoneStdName(0x68, 4),
    TimeZoneDltName(0x5C, 4),
    UNKNOWN4(0x70, 8),
    TimeZoneStdDate(0x78, 4),
    TimeZoneDltDate(0x7C, 4),
    UNKNOWN5(0x80, 8),
    TimeZoneStdBias(0x88, 4),
    TimeZoneDltBias(0x8C, 4),
    LanguageID(0x90, 4),
    VideoFlags(0x94, 4),
    AudioFlags(0x98, 4),
    ParentalControlGames(0x9C, 4),
    ParentalControlPwd(0xA0, 4),
    ParentalControlMovies(0xA4, 4),
    XBOXLiveIPAddress(0xA8, 4),
    XBOXLiveDNS(0xAC, 4),
    XBOXLiveGateWay(0xB0, 4),
    XBOXLiveSubNetMask(0xB4, 4),
    OtherSettings(0xA8, 4),
    DVDPlaybackKitZone(0xBC, 4),
    UNKNOWN6(0xC0, 64)

    final int address
    final int readSize

    Structure(int address, int readSize) {
        this.address = address
        this.readSize = readSize
    }

    /**
     * Converts the structure in to the given class.
     *
     * @param type the @{HasCode} result type.
     * @param eeprom the eeprom to read data from.
     *
     * @return the converted value. <code>null</code> if the value could not be converted.
     */
    def <T extends MatchesEnum> T convertTo(Class<T> type, Eeprom eeprom) {
        def data = eeprom.getRawData(this)
        for (T enumValue : type.enumConstants) {
            if (enumValue.matches(data)) {
                return enumValue
            }
        }
        return null
    }

}