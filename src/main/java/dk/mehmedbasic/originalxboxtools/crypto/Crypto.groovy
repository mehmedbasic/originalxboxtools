package dk.mehmedbasic.originalxboxtools.crypto

import groovy.transform.TypeChecked

@TypeChecked
class Crypto {

    /**
     * Calculates a HMAC for decrypting the eeprom.
     *
     * @param version the Xbox version to attempt.
     * @param messages the data to hash.
     *
     * @return a hashed version of the input.
     */
    static int[] hmac(int version, List<byte[]> messages) {
        def sha1 = new XboxSha1()

        sha1.reset()
        HmacReset.HMAC_1.apply(sha1.context, version)
        for (byte[] array : messages) {
            sha1.input(array, array.length)
        }

        def subResult = sha1.digest()
        for (int i = 0; i < subResult.length; i++) {
            int x = subResult[i];
            sha1.context.messageBlock[i] = x
        }

        sha1.reset()
        HmacReset.HMAC_2.apply(sha1.context, version)
        sha1.input(sha1.context.messageBlock, 0x14);

        sha1.digest()
    }

    /**
     * Call this to calculate the password for the hard drives.
     *
     * @param key the HDD key from the eeprom.
     * @param first the first part of the hashed values.
     * @param second the second part of the hashed values.
     *
     * @return the key as an array of ints.
     */
    static int[] sha1(byte[] key, byte[] first, byte[] second) {
        int[] state1 = new int[0x40]
        int[] state2 = new int[0x40 + 0x14]

        for (int i = 0x40 - 1; i >= key.length; --i) {
            state1[i] = 0x36
        }

        for (int i = key.length - 1; i >= 0; --i) {
            state1[i] = (key[i] & 0xFF) ^ 0x36
        }

        def sha1 = new XboxSha1()
        sha1.reset()
        sha1.input(state1, 0x40)
        sha1.input(first, first.length)
        sha1.input(second, second.length)

        def digest = sha1.digest()
        for (int i = 0; i < 0x14; i++) {
            state2[0x40 + i] = digest[i] & 0xFF
        }

        for (int i = 0x40 - 1; i >= key.length; --i) {
            state2[i] = 0x5C
        }
        for (int i = key.length - 1; i >= 0; --i) {
            state2[i] = (key[i] & 0xFF) ^ 0x5C
        }

        sha1.reset()
        sha1.input(state2, 0x40 + 0x14)
        sha1.digest()
    }
}