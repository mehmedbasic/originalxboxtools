package dk.mehmedbasic.originalxboxtools.crypto

import dk.mehmedbasic.originalxboxtools.util.ArrayConvert
import groovy.transform.TypeChecked


/**
 * A class that calculates SHA-1 and allows the caller to manipulate the inner workings of the hash.
 */
@TypeChecked
class XboxSha1 {
    private static final int SHA1HashSize = 20
    private static int shaSuccess = 0, shaNull = 1, shaStateError = 2

    Context context = new Context()

    int reset() {
        context.lengthLow = 0
        context.lengthHigh = 0
        context.messageBlockIndex = 0

        context.intermediateHash[0] = 0x67452301
        context.intermediateHash[1] = 0xEFCDAB89
        context.intermediateHash[2] = 0x98BADCFE
        context.intermediateHash[3] = 0x10325476
        context.intermediateHash[4] = 0xC3D2E1F0

        context.computed = shaSuccess
        context.corrupted = shaSuccess

        return shaSuccess
    }

    int[] digest() {
        int[] result = new int[SHA1HashSize]
        int i

        if (context.corrupted == shaNull) {
            return result
        }

        if (!context.computed) {
            padMessage()
            for (i = 0; i < 64; ++i) {
                /* message may be sensitive, clear it out */
                context.messageBlock[i] = 0
            }
            context.lengthLow = 0    /* and clear length */
            context.lengthHigh = 0
            context.computed = shaNull

        }

        for (i = 0; i < SHA1HashSize; ++i) {
            def index = i >> 2
            def intermediate = context.intermediateHash[index]
            def l = intermediate >> 8 * (3 - (i & 0x03))
            result[i] = (l & 0xFFFFFFFF).intValue()
        }

        return result
    }

    int input(byte[] message, int length) {
        return input(ArrayConvert.byteToInt(message), length)
    }

    int input(int[] message, int length) {
        if (context.computed != 0) {
            context.corrupted = shaNull

            return shaStateError
        }

        if (context.corrupted != 0) {
            return context.corrupted
        }
        for (int messageIndex = 0; messageIndex < length; messageIndex++) {
            context.messageBlock[context.messageBlockIndex++] = (message[messageIndex] & 0xFF)

            context.lengthLow += 8
            if (context.lengthLow == 0) {
                context.lengthHigh++
                if (context.lengthHigh == 0) {
                    /* Message is too long */
                    context.corrupted = shaNull
                }
            }

            if (context.messageBlockIndex == 64) {
                SHA1ProcessMessageBlock()
            }

            if (context.corrupted != 0) {
                break
            }

        }

        return shaSuccess
    }

    void SHA1ProcessMessageBlock() {
        long[] K = [0x5A827999, 0x6ED9EBA1, 0x8F1BBCDC, 0xCA62C1D6]
        int t
        long temp              /* Temporary word value        */
        long[] W = new long[80]             /* Word sequence               */
        long A, B, C, D, E     /* Word buffers                */

        /*
         *  Initialize the first 16 words in the array W
         */
        for (t = 0; t < 16; t++) {
            W[t] = (context.messageBlock[t * 4] << 24)
            W[t] |= (context.messageBlock[t * 4 + 1] << 16)
            W[t] |= (context.messageBlock[t * 4 + 2] << 8)
            W[t] |= (context.messageBlock[t * 4 + 3])
        }


        for (t = 16; t < 80; t++) {
            W[t] = circularShift(1, W[t - 3] ^ W[t - 8] ^ W[t - 14] ^ W[t - 16])
        }

        A = context.intermediateHash[0]
        B = context.intermediateHash[1]
        C = context.intermediateHash[2]
        D = context.intermediateHash[3]
        E = context.intermediateHash[4]

        for (t = 0; t < 20; t++) {
            temp = circularShift(5, A) + ((B & C) | ((~B) & D)) + E + W[t] + K[0]
            E = D
            D = C
            C = circularShift(30, B)

            B = A
            A = temp
        }

        for (t = 20; t < 40; t++) {
            temp = circularShift(5, A) + (B ^ C ^ D) + E + W[t] + K[1]
            E = D
            D = C
            C = circularShift(30, B)
            B = A
            A = temp
        }

        for (t = 40; t < 60; t++) {
            temp = circularShift(5, A) +
                    ((B & C) | (B & D) | (C & D)) + E + W[t] + K[2]
            E = D
            D = C
            C = circularShift(30, B)
            B = A
            A = temp
        }

        for (t = 60; t < 80; t++) {
            temp = circularShift(5, A) + (B ^ C ^ D) + E + W[t] + K[3]
            E = D
            D = C
            C = circularShift(30, B)
            B = A
            A = temp
        }

        context.intermediateHash[0] += (A & 0xFFFFFFFF).intValue()
        context.intermediateHash[1] += (B & 0xFFFFFFFF).intValue()
        context.intermediateHash[2] += (C & 0xFFFFFFFF).intValue()
        context.intermediateHash[3] += (D & 0xFFFFFFFF).intValue()
        context.intermediateHash[4] += (E & 0xFFFFFFFF).intValue()

        context.messageBlockIndex = 0

    }

    private static long circularShift(long bits, long word) {
        def left = (word & 0xFFFFFFFF) << (bits & 0xFFFFFFFF)
        def right = (word & 0xFFFFFFFF) >>> (32 - (bits & 0xFFFFFFFF))
        (left & 0xFFFFFFFF) | (right & 0xFFFFFFFF)
    }


    private void padMessage() {
        if (context.messageBlockIndex > 55) {
            context.messageBlock[context.messageBlockIndex++] = 0x80
            while (context.messageBlockIndex < 64) {
                context.messageBlock[context.messageBlockIndex++] = 0
            }

            SHA1ProcessMessageBlock()

            while (context.messageBlockIndex < 56) {
                context.messageBlock[context.messageBlockIndex++] = 0
            }
        } else {
            context.messageBlock[context.messageBlockIndex++] = 0x80
            while (context.messageBlockIndex < 56) {

                context.messageBlock[context.messageBlockIndex++] = 0
            }
        }

        /*
         *  Store the message length as the last 8 octets
         */
        context.messageBlock[56] = (context.lengthHigh >> 24).intValue()
        context.messageBlock[57] = (context.lengthHigh >> 16).intValue()
        context.messageBlock[58] = (context.lengthHigh >> 8).intValue()
        context.messageBlock[59] = context.lengthHigh as int
        context.messageBlock[60] = (context.lengthLow >> 24).intValue()
        context.messageBlock[61] = (context.lengthLow >> 16).intValue()
        context.messageBlock[62] = (context.lengthLow >> 8).intValue()
        context.messageBlock[63] = context.lengthLow as int

        SHA1ProcessMessageBlock()
    }


    static class Context {
        long[] intermediateHash = new long[5]
        long lengthLow
        long lengthHigh

        int messageBlockIndex
        int[] messageBlock = new int[64]

        int computed
        int corrupted
    }
}
