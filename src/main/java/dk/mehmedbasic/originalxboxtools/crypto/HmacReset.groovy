package dk.mehmedbasic.originalxboxtools.crypto

enum HmacReset {
    HMAC_1([
            9 : [0x85F9E51A, 0xE04613D2, 0x6D86A50C, 0x77C32E3C, 0x4BD717A4],
            10: [0x72127625, 0x336472B9, 0xBE609BEA, 0xF55E226B, 0x99958DAC],
            11: [0x39B06E79, 0xC9BD25E8, 0xDBC6B498, 0x40B4389D, 0x86BBD7ED],
            12: [0x8058763a, 0xf97d4e0e, 0x865a9762, 0x8a3d920d, 0x08995b2c]]),
    HMAC_2([
            9 : [0x5D7A9C6B, 0xE1922BEB, 0xB82CCDBC, 0x3137AB34, 0x486B52B3],
            10: [0x76441D41, 0x4DE82659, 0x2E8EF85E, 0xB256FACA, 0xC4FE2DE8],
            11: [0x9B49BED3, 0x84B430FC, 0x6B8749CD, 0xEBFE5FE5, 0xD96E7393],
            12: [0x01075307, 0xa2f1e037, 0x1186eeea, 0x88da9992, 0x168a5609]])

    final Map<Integer, List<Long>> hashes

    HmacReset(Map<Integer, List<Long>> map) {
        this.hashes = Collections.unmodifiableMap(map)
    }

    void apply(XboxSha1.Context context, int version) {
        if (!hashes.containsKey(version)) {
            throw new IllegalArgumentException("Unknown version $version for $this")
        } else {
            for (int i = 0; i < hashes[version].size(); i++) {
                long hash = hashes[version][i] as long
                context.intermediateHash[i] = hash
            }
        }
        context.lengthLow = 512
    }

    static int[] versions() {
        return [9, 10, 11, 12]
    }
}
