package dk.mehmedbasic.originalxboxtools.crypto

import dk.mehmedbasic.originalxboxtools.util.ArrayConvert
import groovy.transform.TypeChecked

/**
 * RC4 implementation, ported from xboxhdm hdtool.
 */
@TypeChecked
class Rc4 {
    private final State state = new State()
    int[] keyData

    Rc4(int[] keyData) {
        this.keyData = keyData
        prepareKey()
    }

    byte[] crypt(byte[] buffer) {
        int[] temp = ArrayConvert.byteToInt(buffer)
        cryptInner(temp)
        return ArrayConvert.intToByte(temp)
    }


    private void cryptInner(int[] buffer) {
        int buffer_len = buffer.length
        int x = state.x
        int y = state.y

        int xorIndex
        int counter

        for (counter = 0; counter < buffer_len; counter++) {
            x = (x + 1) % 256
            y = (state.state[x] + y) % 256

            swapBytes(state.state, x, y)
            xorIndex = (state.state[x] + state.state[y]) % 256
            buffer[counter] ^= state.state[xorIndex]
        }
        state.x = x
        state.y = y
    }


    void prepareKey() {
        int index1 = 0
        int index2 = 0
        state.reset()

        for (int counter = 0; counter < 256; counter++) {
            index2 = (keyData[index1] + (state.state[counter] + index2)) % 256
            swapBytes(state.state, counter, index2)

            index1 = (index1 + 1) % keyData.length
        }
    }

    private static void swapBytes(int[] array, int x, int y) {
        int temp = array[x]
        array[x] = array[y]
        array[y] = temp
    }

    /**
     * The inner state of the RC4 algorithm
     */
    static final class State {
        int[] state = new int[256]
        int x
        int y

        void reset() {
            x = 0
            y = 0
            for (int counter = 0; counter < 256; counter++) {
                state[counter] = counter
            }

        }
    }
}