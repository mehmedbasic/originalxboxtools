package dk.mehmedbasic.originalxboxtools.util

import groovy.transform.TypeChecked

import java.nio.ByteBuffer
import java.nio.IntBuffer

/**
 * A small class for converting primitive arrays.
 */
@TypeChecked
class ArrayConvert {
    static int[] byteToInt(byte[] input) {
        def result = IntBuffer.allocate(input.length)
        for (byte value : input) {
            result.put(value & 0xFF)
        }
        result.array()
    }

    static byte[] intToByte(int[] input) {
        def result = ByteBuffer.allocate(input.length)
        for (int value : input) {
            result.put(value as byte)
        }
        result.array()

    }
}
