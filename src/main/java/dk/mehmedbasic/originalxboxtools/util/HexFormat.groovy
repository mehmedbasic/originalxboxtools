package dk.mehmedbasic.originalxboxtools.util

import groovy.transform.TypeChecked

@TypeChecked
class HexFormat {
    static String ints(int[] input) {
        innerByteToString(ArrayConvert.intToByte(input), "%02X")
    }

    static String intsLowerCase(int[] input) {
        innerByteToString(ArrayConvert.intToByte(input), "%02x")
    }

    static String bytes(byte[] input) {
        innerByteToString(input, "%02X")
    }

    static String bytesLowerCase(byte[] input) {
        innerByteToString(input, "%02x")
    }

    static String innerByteToString(byte[] input, String format) {
        input.collect({ byte it ->
            def value = (it & 0xFF) as int
            String.format(format, value)
        }).join("")
    }
}
