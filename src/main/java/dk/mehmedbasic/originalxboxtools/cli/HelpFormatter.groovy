package dk.mehmedbasic.originalxboxtools.cli

import groovy.transform.TypeChecked
import joptsimple.BuiltinHelpFormatter
import joptsimple.OptionDescriptor

@TypeChecked
class HelpFormatter extends BuiltinHelpFormatter {
    private static final int WIDTH = 75

    private final String toolName

    List<String> sortOrder = []
    List<String> spacesBefore = []

    HelpFormatter(String toolName) {
        super(WIDTH, 2)
        this.toolName = toolName
    }

    @Override
    String format(Map<String, ? extends OptionDescriptor> options) {
        addNonOptionRow(VersionInformation.createVersionInfo(toolName, WIDTH))

        List<OptionDescriptor> sorted = []
        sorted.addAll(options.values());
        Collections.sort(sorted, new Comparator<OptionDescriptor>() {
            @Override
            int compare(OptionDescriptor o1, OptionDescriptor o2) {
                return Integer.compare(sortOrder.indexOf(o1.options()[0]), sortOrder.indexOf(o2.options()[0]))
            }
        })

        addRows(sorted);
        def output = formattedHelpOutput()
        for (String property : spacesBefore) {
            if (property.size() == 1) {
                output = output.replace("-$property", "\n-$property")
            } else {
                output = output.replace("--$property", "\n--$property")
            }
        }
        return output
    }
}
