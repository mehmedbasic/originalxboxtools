package dk.mehmedbasic.originalxboxtools.cli

import groovy.transform.CompileStatic

@CompileStatic
class VersionInformation {
    private static final String VERSION = "1.0"

    static String createVersionInfo(String tool, int width) {
        def copyRight = " v$VERSION (c) 2017 Jesenko Mehmedbasic"
        def preamble = "$tool"

        def space = width - copyRight.length()
        preamble.padRight(space) + copyRight
    }
}
